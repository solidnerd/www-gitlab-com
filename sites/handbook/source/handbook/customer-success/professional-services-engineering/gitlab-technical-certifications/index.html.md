---
layout: handbook-page-toc
title: "GitLab Technical Certifications"
description: "Explore how GitLab Professional Services certifies engineers to validate their readiness to deliver Consulting Services offerings."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab Technical Certifications

GitLab offers technical certifications to help the GitLab community and team members validate their ability to apply GitLab in their daily DevOps work. To earn certification, candidates must first pass a written assessment, followed by a hands-on lab assessment graded by GitLab Professional Services engineers. 

### Getting Started

To start your GitLab technical certification journey, you can sign up for an instructor-led [GitLab Certified Associate](https://about.gitlab.com/services/education/gitlab-certified-associate/) program or the [GitLab Certified Associate Self-Service](https://about.gitlab.com/services/education/gitlab-certified-associate-self-service/) option.

Once you've completed the GitLab Certified Associate, you can move on to Specialist certifications. 

### Overview

GitLab is planning and developing several technical certifications to help the GitLab community and team members validate their ability to apply GitLab in their daily DevOps work. To earn certification, candidates must first pass a written assessment, followed by a hands-on lab assessment graded by GitLab Professional Services engineers. 

#### Certification Journey Structure and Flow

##### Structure

There are three levels of GitLab Technical Certification.

1. Associate
    * GitLab Certified Associate
2. Specialist
    * GitLab Certified Specialist: CI/CD
    * GitLab Certified Specialist: Project Management
    * GitLab Certified Specialist: Security
3. Professional
    * GitLab Certified DevOps Professional certification

The Associate level is a prerequisite for the Specialist level, and the Specialist level is a prerequisite for the Professional level.

##### Flow

The following diagram shows the journey from Associate through Professional for the GitLab Certified DevOps Professional certificate.

```mermaid

  flowchart LR
    subgraph a [Specialist Curriculum]
      cd_ci>GitLab CI/CD<br/> Specialist Certification]
      pgm>GitLab Progam Management<br/> Specialist Certification]
      sec>GitLab Security<br/> Specialist Certification]
    end
    gca>GitLab Certified<br/>Associate Certification]-->a
    a-->devops{GitLab DevOps<br/> Professional Certificate}

    classDef orange fill:#f96,stroke:#333,stroke-width:2px;
    classDef fuschia fill:#FCB,stroke:#333,stroke-width:2px;
    classDef burnt fill:#FFA,stroke:#333,stroke-width:2px;
    class gca orange
    class cd_ci fuschia
    class pgm fuschia
    class sec fuschia
    class devops burnt
```
### Why certification?

#### For employers
Team managers now have a way to confirm their team members possess the skills needed to effectively use GitLab in their daily DevOps tasks. This helps ensure the team will be able to successfully adopt GitLab and make the most of the organization's investment.

#### For individuals

Individual GitLab users who earn certification receive a certification logo they can share on social media to showcase their accomplishment. This helps highlight to colleagues and employers their proficiency in effectively using the GitLab platform.

### Currently available certifications

Here are the certifications GitLab have been fully launched in FY'21. Each set of certification assessments is currently available to [GitLab Professional Services](/handbook/customer-success/professional-services-engineering/) customers who purchase the related course. Course participants gain access to the certification assessments immediately after completing their course sessions. GitLab is planning to offer asynchronous options for anyone to prepare for and take the certification assessments, with online availability targeted for the end of calendar year 2021.

#### GitLab Certified Associate

- Description page: [GitLab Certified Associate](https://about.gitlab.com/services/education/gitlab-certified-associate/)
- Related course: [GitLab with Git Basics](https://about.gitlab.com/services/education/gitlab-basics/)

#### GitLab Certified CI/CD Specialist

- Description page: [GitLab Certified CI/CD Specialist](https://about.gitlab.com/services/education/gitlab-cicd-specialist/)
- Related course: [GitLab CI/CD Training](https://about.gitlab.com/services/education/gitlab-ci/)

#### GitLab Certified InnerSourcing Specialist

- Description page: [GitLab Certified InnerSourcing Specialist](https://about.gitlab.com/services/education/gitlab-innersourcing-specialist/)
- Related course: [GitLab InnerSourcing Training](https://about.gitlab.com/services/education/innersourcing-course/)

#### GitLab Certified Project Management Specialist

- Description page: [GitLab Certified Project Management Specialist](https://about.gitlab.com/services/education/gitlab-project-management-specialist/)
- Related course: [GitLab for Project Managers Training](https://about.gitlab.com/services/education/pm/)

#### GitLab Certified Security Specialist

- Description page: [GitLab Certified Security Specialist](https://about.gitlab.com/services/education/gitlab-security-specialist/)
- Related course: [GitLab Security Essentials Training](https://about.gitlab.com/services/education/security-essentials/)

#### GitLab Certified DevOps Professional

- Description page: [GitLab Certified DevOps Professional](https://about.gitlab.com/services/education/gitlab-certified-devops-pro/)
- Related course: [GitLab DevOps Fundamentals Training](https://about.gitlab.com/services/education/devops-fundamentals/)

### Recertification

GitLab Technical Certifications do not have an expiration date. Instead, GitLab uses a continuous model for recertification in which a certified individual can take the latest assessments at any time to receive an updated digital badge. The badge provides verification of the latest date certified for an efficient way to showcase staying up-to-date.

