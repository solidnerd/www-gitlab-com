---
layout: handbook-page-toc
title: Growth Sub-department
description: "The Growth Sub-department consists of development teams building growth experiments and managing privacy focused product intelligence"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

Develop a respectful and privacy focused data collection framework, and an experimentation platform, that allow us to make informed product decisions and improve the customers experience, promote conversion, adoption of key stages, and increase our user base.

## Mission

The Growth sub-department consists of groups that eliminate barriers between our users and our product value.

* [Product Intelligence](product-intelligence/)
* [Experimentation](experimentation/)
* [Activation](activation/)
* [Adoption](adoption/)
* [Conversion](conversion/)
* [Expansion](expansion/)

### Product Intelligence

**Deliver product intelligence data to improve the GitLab product**

[Product Intelligence Group](/handbook/engineering/development/growth/product-intelligence/): Product Intelligence focuses on providing GitLab's team with data-driven product insights to build a better GitLab. To do this, we build data collection and analytics tools within the GitLab product in a privacy-focused manner. Insights generated from Product Intelligence enables us to identify the best places to invest people and resources, what product categories mature faster, where our user experience can be improved, and how product changes impact the business. You can learn more about what we're building next on the [Product Intelligence Direction page](/direction/product-intelligence/).

### Experimentation

**Build an experimentation platform for GitLab to make it easier to run experiments and make data driven product decisions**

[Experimentation](/handbook/engineering/development/growth/experimentation/): Experimentation focuses on providing GitLab's team with an experimentation platform to help build a better GitLab.

### Activation, Adoption, Conversion, and Expansion

**Drive value for the business and our users by improving trial to paid conversion, per-stage adoption, and wider usage of GitLab**

We focus on validating ideas with data across the following four Groups:

- [Activation Group](/handbook/engineering/development/growth/activation/)
- [Adoption Group](/handbook/engineering/development/growth/adoption/)
- [Conversion Group](/handbook/engineering/development/growth/conversion/)
- [Expansion Group](/handbook/engineering/development/growth/expansion/)

The Growth groups also provide development input into two Growth product areas of focus:

- [Registration Flow](https://gitlab.com/gitlab-org/gitlab/-/issues/299983/)
- [Continuous onboarding](https://gitlab.com/gitlab-org/gitlab/-/issues/323176)

We work on the issues prioritized by our product teams including running [experiments](/handbook/engineering/development/growth/experimentation/) on GitLab.com.
More information on priorities can be found on the [Growth direction](/direction/growth/) page.

Unlike most other teams in the engineering division the Growth sub-department has Fullstack Engineers.
The reason for this is that the Growth sub-department has a need for both Frontend and Backend skill-sets,
but as a small team, has optimized for team member efficiency to adopt the Fullstack role.

Some useful links to see how and what we are working on include:

- [Experimentation](experimentation/)
- [GLEX](https://gitlab.com/gitlab-org/ruby/gems/gitlab-experiment)
- [Growth direction](/direction/growth/)
- [Growth group issues](https://gitlab.com/groups/gitlab-org/-/boards/1392106?&label_name%5B%5D=devops%3A%3Agrowth)
- [Experiment rollout](https://gitlab.com/groups/gitlab-org/-/boards/1352542?label_name[]=experiment-rollout)

## Who Are We?

### People leaders in development

<%= shared_team_members(role_regexps: [/(Director of Engineering, Growth, Fulfillment, and Applied ML|Senior Engineering Manager, Growth|Fullstack Engineering Manager, Product Intelligence)/i]) %>

### All Team Members

The following people are permanent members of groups that belong to the Growth sub-department:

#### Activation
<%= department_team(base_department: "Activation Team") %>

#### Adoption
<%= department_team(base_department: "Adoption Team") %>

#### Conversion
<%= department_team(base_department: "Conversion Team") %>

#### Expansion
<%= department_team(base_department: "Expansion Team") %>

#### Product intelligence

##### Product Intelligence Backend
<%= department_team(base_department: "Product Intelligence Team") %>

##### Product Intelligence Frontend
<%= department_team(base_department: "Product Intelligence FE Team") %>

### Business Continuity - Coverage and Escalation

The following table shows who will provide cover if one or more of the Growth Engineering management team are unable to work for any reason.

| Team Member     | Coverered by           | Escalation     |
| -----           | -----                  | -----          |
| Wayne Haber     | Christopher Lefelhocz  | Eric Johnson   |
| Nicolas Dular   | Phil Calder            | Wayne Haber    |
| Phil Calder     | Nicolas Dular          | Wayne Haber    |

If an Engineer is unavailable the Engineering Manager will reassign open issues and merge requests to another engineer, preferably in the same [group](#all-team-members).

Some people management functions may require escalation or delegation, such as BambooHR and Expensify.

This can be used as the basis for a business continuity plan (BCP),
as well as a general guide to Growth Engineering continuity in the event of one or more team members being unavailable for any reason.

## Counterparts

We work closely with the [Data Team](/handbook/business-ops/data-team/) along with our [Product Team](/handbook/product/categories/#growth-stage)
counterparts to design and implement experiments that measure the impact of changes to our messaging, UX, and overall experience of using GitLab.

### Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%=
role_regexp = /[,&] (Growth|Product Intelligence)/
direct_manager_role = 'Director of Engineering, Growth, Fulfillment, and Applied ML'
other_manager_roles = [
  'Interim Fullstack Engineering Manager, Product Intelligence',
  'Interim Senior Engineering Manager, Growth'
]
stable_counterparts(role_regexp: role_regexp, direct_manager_role: direct_manager_role, other_manager_roles: other_manager_roles)
%>

## How We Work

The Growth sub-department uses the `~"devops::growth"` label and the following groups for tracking merge request rate and ownership of issues and merge requests.

| Group name                                 | Group label                   |
| ----------                                 | -----------                   |
| [Product Intelligence](product-intelligence/)    | `~"group::product intelligence"` |
| [Activation](activation/)                  | `~"group::activation"`        |
| [Adoption](adoption/)                      | `~"group::adoption"`          |
| [Conversion](conversion/)                  | `~"group::conversion"`        |
| [Expansion](expansion/)                    | `~"group::expansion"`         |

### Product Development Flow

Our team follows the [Product Development Flow](/handbook/product-development-flow/#workflow-summary) utilizing all labels from `~workflow::start` to `~workflow::verification`.

We adhere to the **Completion Criteria** and **Who Transitions Out** outlined in the [Product Development Flow](/handbook/product-development-flow/#workflow-summary) to progress issues from one stage to the next.

### Workflow Boards

We use workflow boards to track issue progress throughout a milestone. Workflow boards should be viewed at the highest group level for visibility into all nested projects in a group.

There are three GitLab groups we use:
- The [gitlab.com/gitlab-org](https://gitlab.com/gitlab-org/) group includes the [gitlab](https://gitlab.com/gitlab-org/gitlab), [customers-gitlab-com](https://gitlab.com/gitlab-org/customers-gitlab-com), and [license-gitlab-com](https://gitlab.com/gitlab-org/license-gitlab-com) projects.
- The [gitlab.com/gitlab-com](https://gitlab.com/gitlab-com/) group includes the [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com) project.
- The [gitlab.com/gitlab-services](https://gitlab.com/gitlab-services/) group includes the [version-gitlab-com](https://gitlab.com/gitlab-services/version-gitlab-com) project.

| gitlab-org | gitlab-com | gitlab-services | all groups |
| ------ | ------ | ------ | ------ |
| [Growth Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth) | [Growth Workflow](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth) |
| [Activation Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aactivation) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aactivation) | [Activation Workflow](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aactivation) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aactivation) |
| [Conversion Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) | [Conversion Workflow](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) |
| [Expansion Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) | [Expansion Workflow](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) |
| [Adoption Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aadoption) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aadoption) | [Adoption Workflow](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aadoption) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aadoption) |
| [Product Intelligence Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aproduct%20intelligence) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aproduct%20intelligence) | [Product Intelligence Workflow](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aproduct%20intelligence) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aproduct%20intelligence) |
| [Experiment tracking](https://gitlab.com/groups/gitlab-org/-/boards/1352542) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1542208) | - | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=experiment-rollout) |
| [Feature flags](https://gitlab.com/groups/gitlab-org/-/boards/1725470?&label_name[]=devops%3A%3Agrowth&label_name[]=feature%20flag) | - | - | - |

### Estimation

Before work can begin on an issue, we should estimate it first after a preliminary investigation. This is normally done in the monthly planning meeting.

| Weight | Description (Engineering) |
| ------ | ------ |
| 1 | The simplest possible change. We are confident there will be no side effects. |
| 2 | A simple change (minimal code changes), where we understand all of the requirements. |
| 3 | A simple change, but the code footprint is bigger (e.g. lots of different files, or tests effected). The requirements are clear. |
| 5 | A more complex change that will impact multiple areas of the codebase, there may also be some refactoring involved. Requirements are understood but you feel there are likely to be some gaps along the way. |
| 8 | A complex change, that will involve much of the codebase or will require lots of input from others to determine the requirements.
| 13| A significant change that may have dependencies (other teams or third-parties) and we likely still don't understand all of the requirements. It's unlikely we would commit to this in a milestone, and the preference would be to further clarify requirements and/or break in to smaller Issues.

In planning and estimation, we value [velocity over predictability](/handbook/engineering/#velocity-over-predictability). The main goal of our planning and estimation is to focus on the [MVC](/handbook/values/#minimal-viable-change-mvc), uncover blind spots, and help us achieve a baseline level of predictability without over optimizing. We aim for 70% predictability instead of 90%. We believe that optimizing for velocity (merge request rate) enables our Growth teams to achieve a [weekly experimentation cadence](/handbook/product/growth/#weekly-growth-meeting).

- If an issue has many unknowns where it's unclear if it's a 1 or a 5, we will be cautious and estimate high (5).
- If an issue has many unknowns, we can break it into two issues. The first issue is for research, also referred to as a [Spike](https://en.wikipedia.org/wiki/Spike_(software_development)), where we de-risk the unknowns and explore potential solutions. The second issue is for the implementation.
- If an initial estimate is incorrect and needs to be adjusted, we revise the estimate immediately and inform the Product Manager. The Product Manager and team will decide if a milestone commitment needs to be adjusted.

### Product Development Timeline

Our work is planned and delivered on a monthly cycle using [milestones](https://docs.gitlab.com/ee/user/project/milestones/). Our team follows the [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline) utilizing all dates including from `M-1, 4th: Draft of the issues` to `M+1, 4th: Public Retrospective`.

### Milestone Boards

We use milestone boards for high level planning and roadmapping across several milestones.

| gitlab-org | gitlab-com | gitlab-services |
| ------ | ------ | ------ |
| [Growth Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1547281?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth) | [Growth Milestones](https://gitlab.com/groups/gitlab-services/-/boards/1547332?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth) |
| [Activation Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aactivation) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1547281?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aactivation) | [Activation Milestones](https://gitlab.com/groups/gitlab-services/-/boards/1547332?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aactivation) |
| [Conversion Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1547281?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) | [Conversion Milestones](https://gitlab.com/groups/gitlab-services/-/boards/1547332?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) |
| [Expansion Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1547281?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) | [Expansion Milestones](https://gitlab.com/groups/gitlab-services/-/boards/1547332?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion) |
| [Adoption Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aadoption) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1547281?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aadoption) | [Adoption Milestones](https://gitlab.com/groups/gitlab-services/-/boards/1547332?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aadoption) |
| [Product Intelligence Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aproduct%20intelligence) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1547281?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aproduct%20intelligence) | [Product Intelligence Milestones](https://gitlab.com/groups/gitlab-services/-/boards/1547332?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aproduct%20intelligence) |

### UX
Info on the Growth UX team and how they work can be found on the [Product/Growth page](/handbook/product/growth/#ux).

### How We Use Issues

To help our team be [efficient](/handbook/values/#efficiency), we explicitly define how our team uses issues.

#### Issue Creation

We aim to create issues in the same project as where the future merge request will live. For example, if an experiment is being run in the [GitLab CustomersDot](https://customers.gitlab.com), both the issue and MR should be created in the [CustomersDot](https://gitlab.com/gitlab-org/customers-gitlab-com) project.

We emphasize creating the issue in the right project to avoid having to close and move issues later in the development process. If the location of the future merge request cannot be determined, we will create the issue in our catch-all [growth team-tasks project](https://gitlab.com/gitlab-org/growth/team-tasks/issues).

We use [issue templates](experimentation/#experiment-issue-templates) for common tasks.

#### Iteration

To support [Iteration](/handbook/values/#iteration) Growth engineering:

1. Separates refactoring from feature [MVCs](/handbook/values/#minimal-viable-change-mvc). When refactoring is raised in review, the preference is to resolve in a follow up issue.
1. Addresses technical debt and follow up issues through prioritization and discussion by relevant stakeholders.
1. Issues with a weight of `5` and higher should be reassigned to the Product Manager to make sure they can be split into smaller [MVCs](/handbook/values/#minimal-viable-change-mvc).
   When this is not possible, the Product Manager will create a spike or research issue so that engineering can break it down and close the original.
1. We make use of research issues and engineering spikes in the prior milestone, so estimations are more accurate.

#### Status Updates

The DRI (assignee) of an issue is encouraged to add an async issue update, particularly for issues labelled `~Deliverable` or `~Growth-Deliverable`.
These updates can be added anytime and are useful to highlight when an issue is on track, blocked, or may need to be re-prioritized.
This helps us maintain [transparency](/handbook/values/#transparency) and our [bias towards asynchronous communication](/handbook/values/#bias-towards-asynchronous-communication).

```
Async issue update

**YYYY-MM-DD Update**

#### Please provide a quick summary of the current status (one sentence).

#### When do you predict this feature to be ready for maintainer review?

#### Are there any opportunities to further break the issue or merge request into smaller pieces (if applicable)?

#### Were expectations met from a previous update? If not, please explain why.

```

### Experimentation

The Growth groups regularly run experiments to test product hypothesis.

- Read about the [experimentation process](./experimentation).
- View the [developer documentation](https://docs.gitlab.com/ee/development/experiment_guide/)
- View slides for the [Experimentation Workshop](https://docs.google.com/presentation/d/1nmStWChWkYad9K-dced9wS4jS7XLIrHB-WKafc7jrMU/)

GitLab team members are welcome to attend the [Growth Engineering Weekly](#growth-engineering-weekly) meetings
to connect with growth team members and find out more about running experiments at GitLab.

### Growth metrics

The Growth sub-department experiment teams track the number of closed "growth deliverable" issues prioritized by our product managers using the [Growth-Deliverable definition](/handbook/product/growth/#growth-deliverables).
This metric acts as a target for deliverables based on number of engineers available to the four Growth experiment teams.

<% if ENV['PERISCOPE_EMBED_API_KEY'] %>
  <div>
    <embed width="100%" height="375" src="<%= signed_periscope_url(chart: 12514149, dashboard: 799224, embed: 'v2') %>">
  </div>
<% else %>
You must set a <code>PERISCOPE_EMBED_API_KEY</code> environment variable to render this chart.
<% end %>

The following view shows the breakdown of closed Growth issues by type. This highlights the percentage of issues that relate to experiments or features.

<% if ENV['PERISCOPE_EMBED_API_KEY'] %>
  <div>
    <embed width="100%" height="375" src="<%= signed_periscope_url(chart: 12917708, dashboard: 799224, embed: 'v2') %>">
  </div>
<% else %>
You must set a <code>PERISCOPE_EMBED_API_KEY</code> environment variable to render this chart.
<% end %>

The Growth sub-department tracks number of experiments per engineer as a development metric.
This counts new experiments added to the codebase using [experiment feature flags](https://docs.gitlab.com/ee/development/feature_flags/#experiment-type).
Current experiments shows the total number of experiments in the codebase (pending, active, or concluded and ready to be removed).

<% if ENV['PERISCOPE_EMBED_API_KEY'] %>
  <div>
    <embed width="100%" height="575" src="<%= signed_periscope_url(chart: 10621867, dashboard: 799224, embed: 'v2') %>">
  </div>
<% else %>
You must set a <code>PERISCOPE_EMBED_API_KEY</code> environment variable to render this chart.
<% end %>

#### Engineering Metrics

* [Growth Section engineering metrics]

## Growth Engineering Weekly

Every week, engineers in the Growth sub-department meet to discuss topics related to growth engineering.
Discussion topics include how to track experiments, A/B testing, product intelligence and data concerns, and changes at gitlab affecting engineering.
Growth Engineers are encouraged to bring discussion topics to the meeting and add them to the [agenda](https://docs.google.com/document/d/1VMj16-tvJg4m26y6q7A1jSdBD895ImFM2fbXvFXF4yM/edit?usp=sharing).

To get the most time zone coverage, these meetings alternate fortnightly between:
* Wednesday 1:00 pm CET for EMEA
* Wednesdays 9:30 am PT for AMER

Ad-hoc meetings are also arranged at 8:00 am CET. Team members in different parts of APAC should be able to attend at least one of the three meetings.

Team members are encouraged to contribute to the agenda async and attend the meeting that matches their time zone.

## Team Days

On occasion we put together a virtual team day to take a break and participate in fun, social activities across our Product, Development, UX, Data and Quality functions.

* [April 2021](https://gitlab.com/gitlab-org/growth/product/-/issues/1675)
* [September 2020](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/175)
* [May 2020](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/119)

## Common Links

* [How to create a Sisense SQL alert](/handbook/engineering/development/growth/sisense_alert.html)
* [Growth sub-department]
* [Growth workflow board]
* `#s_growth` in [Slack](https://gitlab.slack.com/archives/s_growth) (GitLab internal)
* [Product Intelligence issues board]
* `#g_product_intelligence` in [Slack](https://gitlab.slack.com/archives/g_product_intelligence) (GitLab internal)
* [Growth technical debt status](https://app.periscopedata.com/app/gitlab/618368/Growth-technical-debt-status) (GitLab internal)
* [Growth opportunities]
* [Growth meetings and agendas] (GitLab internal)
* [GitLab values]

[Experiment idea]: https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Experiment%20Idea

[GitLab values]: /handbook/values/
[Growth sub-department]: /handbook/engineering/development/growth/
[Growth workflow board]: https://gitlab.com/groups/gitlab-org/-/boards/1158847
[Growth opportunities]: https://gitlab.com/gitlab-org/growth/product/-/issues
[Growth meetings and agendas]: https://drive.google.com/drive/search?q=type:document%20title:%22Growth%20Weekly%22
[Growth section engineering metrics]: /handbook/engineering/metrics/growth/
[Growth OKRs]: https://gitlab.com/gitlab-com/www-gitlab-com/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Growth%20Sub-Department&label_name[]=OKR
[Product Intelligence issues board]: https://gitlab.com/gitlab-org/product-intelligence/-/issues
