---
layout: handbook-page-toc
title: "GitLab's Customer Assurance Activities"
---
<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Customer Assurance Activity Requests

It's no surprise that GitLab Customers and Prospects conduct Security due diligence activities prior to contracting with GitLab. We recognize the importance of these reviews and have designed this procedure for **GitLab Team Members** to request Customer Assurance Activities. 

<div class="flex-row" markdown="0" style="height:80px">
       <a href="https://gitlab.com/gitlab-com/gl-security/security-assurance/field-security-team/customer-assurance-activities/caa-servicedesk/-/issues/new?issuable_template=Questionnaire%20CAA" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Questionnaire</a>
       <a href="https://gitlab.com/gitlab-com/gl-security/security-assurance/field-security-team/customer-assurance-activities/caa-servicedesk/-/issues/new?issuable_template=Contract%20Review%20CAA" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Contract Review</a>
    <a href="https://gitlab.com/gitlab-com/gl-security/security-assurance/field-security-team/customer-assurance-activities/caa-servicedesk/-/issues/new?issuable_template=Customer%20Call%20CAA" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Customer Call</a>
    <a href="https://gitlab.com/gitlab-com/gl-security/security-assurance/field-security-team/customer-assurance-activities/caa-servicedesk/-/issues/new?issuable_template=External%20Evidence%20CAA" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">External Evidence</a>
</div>   

The above processes are for GitLab Team Members only. Customers should contact their GitLab [Account Owner](/handbook/sales/#initial-account-owner---based-on-segment) to initiate their requests. If a customer doesn't know their Account Owner or does not yet have an assigned Account Owner, they can [contact the sales team](https://about.gitlab.com/sales/). 

---

## Public Documentation

* Search for [General Information about GitLab](https://about.gitlab.com) in our public handbook.
* Review [GitLab's Product Security Documentation](https://docs.gitlab.com).
* Review [GitLab's Customer Assurance Package](https://about.gitlab.com/security/cap/) and download publically available security assurance documents. To request our `NDA Required` Customer Assurance Pacakge (that includes our SOC2 report), utilize the `Request by Email` option. 

## AnswerBase

GitLab Team Members have access to our **internal** database of commonly asked questions and answers, [**GitLab AnswerBase**](/handbook/engineering/security/security-assurance/field-security/answerbase.html). We ask that prior to submitting a new request, GitLab team members utilize this tool and answer any questions they can. If the answer can't be found in the **GitLab AnswerBase**, GitLab Team Members can:
   * Pose a new question in GitLab AnswerBase
   * Pose a new question in the [#sec-fieldsecurity](https://gitlab.slack.com/archives/CV5A53V70) Slack Channel. 
   * Search for prior questions and answers in Slack using the following format: `[keywords] in:[channel name]`

## Self-Attestations

In the spirit of iteration, GitLab is continuously evolving our list of compliance self-attestations. Completed self-attestations are reviewed annually for continued applicability and can be found in our [Customer Assurance Package.](https://about.gitlab.com/security/cap/) Customers can submit suggestions and requests for new self-attestations through their Account Manager. GitLab team members can submit recommendations for future compliance assessments through the [Regulatory Security Compliance Feedback and Field Research epic](https://gitlab.com/groups/gitlab-com/gl-security/-/epics/56).

## Service Level Agreements 

- **Security Questionnaires:** 10 Business Day. SA or TAM will utlize AnswerBase and/or other self-service resources prior to requesting Field Security assistance. SA or TAM will ensure everyone on the Field Security team has access to any files or portals.

- **Contract Reviews:** 5 Business Days. The VP of Security must be engaged in all Contract Reviews. 

- **Customer Calls:** SA or TAM will provide context to the Customer or Prospects questions or concerns prior to the meeting. Field Security will provide a PowerPoint presentation with critical information about GitLab Security and specifics to the Customer or Prospect's request. The VP of Security must be invited to all Customer Meetings. 

- **External Evidence:** 2 Business Days. SA or TAM must provide the name and email address of the recipient. 

## Exceptions
If the Account Owner or Customer Success point of contact feel they have sufficient knowledge and resoures to complete a Customer Assessment, this procedure does not have to used. These excpetions, will not be tracked. 

<div class="flex-row" markdown="0" style="height:40px">
    <a href="https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/" class="btn btn-purple-inv" style="width:100%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Return to the Field Security Homepage</a>
</div> 
