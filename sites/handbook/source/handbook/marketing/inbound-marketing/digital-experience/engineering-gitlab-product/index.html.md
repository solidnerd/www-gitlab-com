---
layout: handbook-page-toc
title: Engineering GitLab Product 
description: >-
  Learn more about how Digital Experience engineers work with the GitLab Product.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Engineering GitLab Product

## Overview

From time to time, our team has objectives that require us to collaborate on the [GitLab product](https://gitlab.com/gitlab-org/gitlab).

### Onboarding

There is an existing onboarding process for GitLab engineering. Digital Experience engineers need to complete a specific subset of that onboarding: 

1. [Make sure you still feel comfortable with day 5 of the general onboarding steps for git workflow](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/master/.gitlab/issue_templates/onboarding.md#day-5-git)
1. [Complete the Engineering Division/For Development Department specific tasks](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/master/.gitlab/issue_templates/onboarding_tasks/department_development.md)
1. [Complete the Snowplow onboarding tasks](/handbook/engineering/development/growth/product-intelligence/#snowplow-onboarding-template)
