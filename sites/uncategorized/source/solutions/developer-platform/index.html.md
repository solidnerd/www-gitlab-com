---
layout: markdown_page
title: "Developer platform"
description: "To succeed in digital economy, your teams need a dedicated development ecosystem to connect, collaborate and create new and innovative products."
canonical_path: "/solutions/developer-platform/"
---

## Introduction

In the world of digital transformation, the products that differentiate your business are digital. Software is the driver of the digital economy and your development teams need to be able to organize, plan, iterate and ship new products.  In many enterprises, the landscape of tools to support developers looks like smorgasborg of tools and technology stitched together.  

To succeed in digital economy, your teams need a dedicated development ecosystem, where they can connect, collaborate and create new and innovative products.   They need to be able to adopt and practice [agile delivery methods](/solutions/agile-delivery/){:data-ga-name="Agile delivery"}{:data-ga-location="body"} such as [scrum](/solutions/agile-delivery/#scrum){:data-ga-name="Scrum"}{:data-ga-location="body"} and [kanban](/solutions/agile-delivery/#kanban){:data-ga-name="Kanban"}{:data-ga-location="body"}


## Benefits of a Team Platform

1. Faster on-boarding and off-boarding team members.
1. Easier to get started, less time waiting for work.
1. Easier to collaborate, share and improve.
1. Able to focus on solving business problems (not distracted with pipeline 'plumbing'
1. Happier and more engaged team - less likely to leave, less churn.
1. Able to communicate new features, expand usage of new features.
1. Able to collaborate and understand market needs and then deliver better products.


## Open issue tracker
GitLab's issue tracker and [agile delivery methods](/solutions/agile-delivery/){:data-ga-name="Agile delivery"}{:data-ga-location="body"} make it easy for teams to collaborate and organize their work to address the real business challenges you face.   In GitLab, not only can developers collaborate, but the entire team from business leaders to IT Ops and security; everyone can contribute and help the team innovate.

## Innersourcing
Collaboration and sharing are key elements of unlocking innovation.   Applying the principles of Open Source development to internal teams is the concept of innersourcing.  Here, the practices of automated testing, code reviews and code reuse help teams to become more efficient and effective at delivering higher quality code at the speed of business.   GitLab's review and approval features allow you to streamline delivering new innovation and sharing across teams.

## PaaS
With every handoff in IT, there is yet another opportunity for friction to slow the business and prevent innovation from reaching your customers.   Because GitLab supports the entire delivery lifecycle and can deploy and manage your applications for you in Kubernetes, your teams can dramatically accelerate their delivery cadence and meet the expectations of the business.   GitLab Auto DevOps can dramatically accelerate the time it takes to get a new application and a new pipeline up and running.

## Security
DevOps without security is unwise, if not dangerous.  Security needs to be an integral part of the delivery lifecycle and should never be left as the last step before production.  GitLab makes it easy to incorporate Software Security Scans with every commit.  This way, each and every change can be scanned and reviewed to determine if it introduces any new vulnerabilities.   In fact, GitLab goes further, including Dynamic Security Scans, Dependency Scans and container scans to help ensure your applications can be trusted.
